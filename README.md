# Connect4 3D

Implementation of a generalized 3D connect 4 game with configurable size.

# Usage

```python
game = Connect43D()

print(game.get_move())

game.do_move((1, 2))

print(game.winner)
print(game.game)
```