from random import choice
from Connect43D import Connect43D

# move test

game = Connect43D(1, 3, 3, 3)
print(game.get_size())

while game.get_move():
    print(game.get_move())
    game.do_move(choice(game.get_move()))

print(game.game)

# end game test

for i in range(1):
    game = Connect43D(1, 3, 3, 3)

    while not game.is_finished():
        game.do_move(choice(game.get_move()))
        print(game.game)

    print(game.winner)
    print(game.game)
